﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.StreamMessages
{
    [DataContract]
    public class PennyUsed : StreamMessage
    {
        [DataMember]
        public int PennyId { get; set; }
    }
}
