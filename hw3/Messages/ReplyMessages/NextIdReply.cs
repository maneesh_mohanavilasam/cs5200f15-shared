﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.ReplyMessages
{
    [DataContract]
    public class NextIdReply : Reply
    {
        [DataMember]
        public int NextId { get; set; }
    }
}
