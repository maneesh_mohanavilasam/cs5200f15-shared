﻿using System;
using System.Runtime.Serialization;

namespace Messages.AuctionMessages
{
    [DataContract]
    public class AuctionStarted : AuctionMessage
    {
        [DataMember]
        public int InitialPrice { get; set; }
    }
}
