﻿using System;
using System.Runtime.Serialization;

using Messages;

namespace Messages.AuctionMessages
{
    [DataContract]
    public class AuctionMessage : Message
    {
        [DataMember]
        public int AuctionId { get; set; }
    }
}
