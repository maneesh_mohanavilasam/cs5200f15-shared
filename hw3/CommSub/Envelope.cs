﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using Messages;
using SharedObjects;

namespace CommSub
{
    public class Envelope
    {
        public Message Message { get; set; }
        
        public PublicEndPoint EP { get; set; }

        public Envelope() {}

        public Envelope(Message message, PublicEndPoint ep)
        {
            Message = message;
            EP = ep;
        }

        public Envelope(Message message, IPEndPoint ep) :
            this(message, (ep != null) ? new PublicEndPoint() { IPEndPoint = ep } : null) {}

        public IPEndPoint IPEndPoint
        {
            get
            {
                return (EP == null) ?
                    new IPEndPoint(IPAddress.Any, 0) :
                    EP.IPEndPoint;
            }
            set
            { 
                if (value == null) EP = null;
                else EP = new PublicEndPoint() { IPEndPoint = value }; }
        }

        public bool IsValidToSend
        {
            get
            {
                return (Message != null &&
                        EP != null &&
                        EP.Host!="0.0.0.0" &&
                        EP.Port!=0);
            }
        }
    }
}
