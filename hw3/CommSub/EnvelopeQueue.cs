﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

using Messages;
using SharedObjects;

using log4net;

namespace CommSub
{
    /// <summary>
    /// This a class defines and tracks queues of envelops.  The queues are thread safe.
    /// 
    /// There is one special queue, called RequestQueue, that is supposed to hold incoming requests.
    /// All other queues are for specific conversations.
    /// 
    /// Use the RequestQueue property or GetByName methods to construct and access queue.
    /// 
    /// Note that a queue needs to disposed when it is no longer needed.  To do this call Dispose();
    /// </summary>
    public class EnvelopeQueue
    {
        #region Private Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(EnvelopeQueue));

        private ConcurrentQueue<Envelope> myQueue = new ConcurrentQueue<Envelope>();
        private ManualResetEvent somethingEnqueued = new ManualResetEvent(false);
        #endregion

        #region Constructors and Destructors
        public EnvelopeQueue() { }

        #endregion

        #region Public Properties

        public MessageNumber QueueId { get; set; }

        public int Count
        {
            get { return myQueue.Count; }
        }

        public void Enqueue(Envelope envelope)
        {
            if (envelope != null)
            {
                myQueue.Enqueue(envelope);
                log.DebugFormat("Enqueued an envelope into queue {0}", QueueId);
                somethingEnqueued.Set();
            }
        }

        public Envelope Dequeue(int timeout)
        {
            Envelope result=null;
            int remainingTime = timeout;
            while (result == null && remainingTime > 0)
            {
                DateTime ts = DateTime.Now;
                if (myQueue.Count == 0)
                    somethingEnqueued.WaitOne(timeout);

                if (myQueue.TryDequeue(out result))
                {
                    somethingEnqueued.Reset();
                    log.DebugFormat("Dequeued an envelope from queue {0}", QueueId);
                }
                else
                    remainingTime -= Convert.ToInt32(DateTime.Now.Subtract(ts).TotalMilliseconds);
            }

            return result;
        }

        #endregion
    }
}
