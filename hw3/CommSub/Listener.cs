﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Utils;
using Messages;

using log4net;

namespace CommSub
{
    public class Listener : BackgroundThread
    {
        #region Private Data Members
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Listener));
        private const int timeout_ms = 1000;
        #endregion

        public Listener() { }

        public CommSubsystem CommSubsystem { get; set; }

        protected override void Process(Object state)
        {
            while (keepGoing)
            {
                Envelope e = CommSubsystem.Communicator.GetIncoming(timeout_ms);
                if (e != null && e.Message!=null)
                {
                    _logger.DebugFormat("Received message: Type={0}, From={1}", e.Message.GetType().Name, e.IPEndPoint.ToString());
                    EnqueueEnvelope(e);
                }
            }
        }

        private void EnqueueEnvelope(Envelope e)
        {
            EnvelopeQueue queue = CommSubsystem.QueueDictionary.GetByName(e.Message.ConversationId);

            if (queue != null)
            {
                _logger.DebugFormat("Placing message in the queue for conversation {0}", e.Message.ConversationId.ToString());
                queue.Enqueue(e);
            }
            else
            {
                if (CommSubsystem.ConversationFactory.IncomingMessageCanStartConversation(e.Message.GetType()))
                {
                    _logger.DebugFormat("Placing message in the request queue");
                    CommSubsystem.QueueDictionary.RequestQueue.Enqueue(e);
                }
                else
                    _logger.WarnFormat("Unexcepted incoming message of type {0}", e.Message.GetType().Name);
            }
        }
    }
}
