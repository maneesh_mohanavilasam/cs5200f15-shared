﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

using Messages;
using SharedObjects;

using log4net;

namespace CommSub
{
    public class EnvelopeQueueDictionary
    {
        #region Private Class Members
        private static readonly ILog log = LogManager.GetLogger(typeof(EnvelopeQueue));

        // Create a special request queue not associated with conversations and will hold incoming requests that will start new conversations.
        private EnvelopeQueue requestQueue = new EnvelopeQueue();
        // Create a dictionary of queues for conversations in progress, plus a lock object for the dictionary
        private ConcurrentDictionary<MessageNumber, EnvelopeQueue> activeQueues =
            new ConcurrentDictionary<MessageNumber, EnvelopeQueue>(new MessageNumber.MessageNumberComparer());
        #endregion

        #region Public Methods
        public EnvelopeQueue RequestQueue
        {
            get { return requestQueue; }
        }

        public EnvelopeQueue CreateQueue(MessageNumber queueId)
        {
            EnvelopeQueue result = null;
            if (queueId != null)
            {
                log.DebugFormat("CreateQueue for key={0}", queueId);
                result = GetByName(queueId);
                if (result == null)
                {
                    result = new EnvelopeQueue() { QueueId = queueId };
                    activeQueues.TryAdd(queueId, result);
                }
            }
            return result;
        }

        public EnvelopeQueue GetByName(MessageNumber queueId)
        {
            log.DebugFormat("GetByName for name={0}", queueId);

            EnvelopeQueue result = null;
            activeQueues.TryGetValue(queueId, out result);

            return result;
        }

        public void CloseQueue(MessageNumber queueId)
        {
            log.DebugFormat("Remove Queue {0}", queueId);
            EnvelopeQueue queue = null;
            activeQueues.TryRemove(queueId, out queue);
        }

        public void ClearAllQueues()
        {
            activeQueues.Clear();
        }

        public int ConversationQueueCount
        {
            get { return activeQueues.Count; }
        }
        #endregion
    }
}
