﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommSub
{
    public abstract class ConversationFactory
    {
        private Dictionary<Type, Type> typeMappings = new Dictionary<Type, Type>();

        public CommSubsystem CommSubsystem { get; set; }
        public CommProcessState ProcessState { get; set; }
        public int DefaultMaxRetries { get; set; }
        public int DefaultTimeout { get; set; }

        public bool IncomingMessageCanStartConversation(Type messageType)
        {
            return typeMappings.ContainsKey(messageType);
        }

        public virtual Conversation Create(Type messageType)
        {
            return CreateFromMessageType(messageType, null);
        }

        public virtual Conversation CreateFromMessageType(Type messageType, Envelope envelope)
        {
            Conversation conversation = null;
            if (messageType != null && typeMappings.ContainsKey(messageType))
                conversation = CreateFromConversationType(typeMappings[messageType], envelope);
            return conversation;
        }

        public virtual Conversation CreateFromConversationType(Type conversationType, Envelope envelope = null)
        {
            Conversation conversation = null;
            if (conversationType!=null)
            {
                conversation = Activator.CreateInstance(conversationType) as Conversation;
                conversation.CommSubsystem = CommSubsystem;
                conversation.ProcessState = ProcessState;
                conversation.MaxRetries = DefaultMaxRetries;
                conversation.Timeout = DefaultTimeout;
                conversation.IncomingEnvelope = envelope;
            }
            return conversation;
        }

        public abstract void Initialize();

        protected void Add(Type messageType, Type conversationType)
        {
            if (messageType != null && conversationType != null && !typeMappings.ContainsKey(messageType))
                typeMappings.Add(messageType, conversationType);
        }
    }
}
