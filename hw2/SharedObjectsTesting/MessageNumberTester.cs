﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SharedObjects;

namespace SharedObjectTesting
{
    [TestClass]
    public class MessageNumberTester
    {
        [TestInitialize]
        public void Setup()
        {
            MessageNumber.LocalProcessId = 100;
        }

        [TestMethod]
        public void MessageNumber_TestEverything()
        {
            MessageNumber.ResetSeqNumber();

            MessageNumber mn0 = new MessageNumber();
            Assert.AreEqual(0, mn0.ProcessId);
            Assert.AreEqual(0, mn0.SeqNumber);
            Assert.AreEqual(0, mn0.GetHashCode());

            MessageNumber mn1 = MessageNumber.Create();
            Assert.AreEqual(100, mn1.ProcessId);
            Assert.AreEqual(1, mn1.SeqNumber);
            Assert.AreEqual(0x00640001, mn1.GetHashCode());

            MessageNumber mn2 = MessageNumber.Create();
            Assert.AreEqual(100, mn1.ProcessId);
            Assert.AreEqual(mn1.SeqNumber + 1, mn2.SeqNumber);

            for (int i = 0; i < 100; i++)
            {
                MessageNumber mn4 = MessageNumber.Create();
                Assert.AreEqual(100, mn1.ProcessId);
                Assert.AreEqual(mn2.SeqNumber + i + 1, mn4.SeqNumber);
            }

            MessageNumber mn3 = new MessageNumber();
            Assert.AreEqual(0, mn3.ProcessId);
            Assert.AreEqual(0, mn3.SeqNumber);

            mn3.ProcessId = mn2.ProcessId;
            mn3.SeqNumber = mn2.SeqNumber;

            Assert.AreEqual(mn2.ProcessId, mn3.ProcessId);
            Assert.AreEqual(mn2.SeqNumber, mn3.SeqNumber);

            Assert.IsTrue(mn2.Equals(mn3));
            Assert.IsFalse(mn2.Equals(mn1));
            Assert.IsFalse(mn2.Equals(null));
            Assert.IsFalse(mn2.Equals(MessageNumber.Create()));

            Assert.IsTrue(mn2 == mn3);
            Assert.IsTrue(mn1 <= mn3);
            Assert.IsTrue(mn1 < mn3);
            Assert.IsTrue(mn3 != mn1);
            Assert.IsTrue(mn3 >= mn1);
            Assert.IsTrue(mn3 > mn1);

            MessageNumber.MessageNumberComparer comparer = new MessageNumber.MessageNumberComparer();
            Assert.IsTrue(comparer.Equals(mn2, mn3));
            Assert.AreEqual(0, comparer.GetHashCode(mn0));
            Assert.AreEqual(0x00640001, comparer.GetHashCode(mn1));
        }

    }
}
