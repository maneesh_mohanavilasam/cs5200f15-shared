# README #

This repository contains shared code for USU's CS5200 class, Fall 2015

### What is this repository for? ###

* To provide share code for CS5200's course project
* Version 1.0

### How do I get set up? ###

* You may fork this repository and use whatever you need to from it
* If you are using C#, you can use the Message and SharedObjects projects directly
* If you are using any other language, you can build similar classes to handle your messages and shared object.  The JSON serialization must be the same as in the C# classes!